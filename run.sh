#!/bin/sh
set -eu
docker stop msqmqtt && docker rm msqmqtt  || true
docker build . -t emqtt
docker run -d -p 1883:1883 -p 9001:9001 --name msqmqtt emqtt
