FROM eclipse-mosquitto:1.5.4
COPY /conf.d/websockets.conf /mosquitto/config/conf.d/
COPY /conf.d/tcp.conf /mosquitto/config/conf.d/
RUN echo "include_dir /mosquitto/config/conf.d" >> /mosquitto/config/mosquitto.conf
